backupFolder=`pwd`/../backups
backupFile=$backupFolder/jd.`date '+%Y-%m-%d-%H-%M'`.sql
mysqldump --login-path=local --databases jd > $backupFile 
gzip $backupFile
ls -la $backupFolder

